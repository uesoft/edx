function mgr:on_java_completion(doc, status, helper)
	-- java 自动补全
	print("java complete ", status);
	-- 得到当前行
	local text = doc.get_text(doc.cursor_line, 0, doc.cursor_column) or "";
	-- 取得最后一部分
	text = text:match("([.:_%w]+)$") or "";
	if status ~= 0 and (text == nil or text == "") then
		helper.cancel_completion();
		lsp:cleanup_completion(doc);
		return 0;
	end ;

	if status == 1 then
		lsp:commit_completion(doc, helper);
		return 1;
	end;

	if status == 5 then
		lsp:update_completion_tip(doc, helper);
		return 1;
	end;

	if status == 0 then
		lsp:do_completion(doc, helper);
		return 1;
	end;
	
	if status == 2 then
		helper.select_item(text);
		helper.redraw();
		return 1;
	end;

	return 0;
end;
